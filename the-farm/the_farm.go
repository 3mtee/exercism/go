package thefarm

import (
	"errors"
	"fmt"
)

type InvalidCowsError struct {
	cows    int
	message string
}

func (err *InvalidCowsError) Error() string {
	return fmt.Sprintf("%v cows are invalid: %v", err.cows, err.message)
}

func DivideFood(calc FodderCalculator, cows int) (float64, error) {
	total, err := calc.FodderAmount(cows)
	if err != nil {
		return 0, err
	}
	fatteningFactor, err := calc.FatteningFactor()
	if err != nil {
		return 0, err
	}
	return total * fatteningFactor / float64(cows), nil
}

func ValidateInputAndDivideFood(calc FodderCalculator, cows int) (float64, error) {
	if cows <= 0 {
		return 0, errors.New("invalid number of cows")
	}
	return DivideFood(calc, cows)
}

func ValidateNumberOfCows(cows int) error {
	if cows < 0 {
		return &InvalidCowsError{cows, "there are no negative cows"}
	}
	if cows == 0 {
		return &InvalidCowsError{cows, "no cows don't need food"}
	}
	return nil
}
