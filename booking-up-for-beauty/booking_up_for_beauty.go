package booking

import (
	"fmt"
	"time"
)

var openingDate = time.Date(2012, time.September, 15, 0, 0, 0, 0, time.UTC)

// Schedule returns a time.Time from a string containing a date.
func Schedule(date string) time.Time {
	// 11/28/1984 2:02:02
	return parseDate("1/2/2006 15:04:05", date)
}

func parseDate(layout, date string) time.Time {
	parse, err := time.Parse(layout, date)
	if err != nil {
		panic("got a parsing error")
	}
	return parse
}

// HasPassed returns whether a date has passed.
func HasPassed(date string) bool {
	return parseDate("January 2, 2006 15:04:05", date).Before(time.Now())
}

// IsAfternoonAppointment returns whether a time is in the afternoon.
func IsAfternoonAppointment(date string) bool {
	// "Thursday, May 13, 2010 20:32:00"
	hour := parseDate("Monday, January 2, 2006 15:04:05", date).Hour()
	return hour >= 12 && hour < 18
}

// Description returns a formatted string of the appointment time.
func Description(date string) string {
	t := parseDate("1/2/2006 15:04:05", date).Format("Monday, January 2, 2006, at 15:04")
	return fmt.Sprintf("You have an appointment on %v.", t)
}

// AnniversaryDate returns a Time with this year's anniversary.
func AnniversaryDate() time.Time {
	return time.Date(time.Now().Year(), openingDate.Month(), openingDate.Day(), 0, 0, 0, 0, time.UTC)
}
