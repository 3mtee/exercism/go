package elon

import "fmt"

func (car *Car) Drive() {
	if car.batteryDrain > car.battery {
		return
	}
	car.battery -= car.batteryDrain
	car.distance += car.speed
}

func (car *Car) DisplayDistance() string {
	return fmt.Sprintf("Driven %v meters", car.distance)
}

func (car *Car) DisplayBattery() string {
	return fmt.Sprintf("Battery at %v%%", car.battery)
}

func (car *Car) CanFinish(trackDistance int) bool {
	return car.battery/car.batteryDrain*car.speed >= trackDistance
}
