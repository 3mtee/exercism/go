// Package weather provides functionality to do weather forecasts.
package weather

// CurrentCondition represents current weather condition.
var CurrentCondition string

// CurrentLocation represents location.
var CurrentLocation string

// Forecast returns a string with a weather condition for a given city.
func Forecast(city, condition string) string {
	CurrentLocation, CurrentCondition = city, condition
	return CurrentLocation + " - current weather condition: " + CurrentCondition
}
