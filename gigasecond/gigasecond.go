package gigasecond

import "time"

// AddGigasecond adds 1_000_000_000 seconds to the given date and returns the resulting value
func AddGigasecond(t time.Time) time.Time {
	return t.Add(1e9 * time.Second)
}
