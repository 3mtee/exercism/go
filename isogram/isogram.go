package isogram

import (
	"strings"
	"unicode"
)

func IsIsogram(word string) bool {
	lower := strings.ToLower(word)
	for i, r := range lower {
		if unicode.IsLetter(r) && strings.ContainsRune(lower[i+1:], r) {
			return false
		}
	}
	return true
}
