package parsinglogfiles

import (
	"fmt"
	"regexp"
)

func IsValidLine(text string) bool {
	re := regexp.MustCompile(`^\[(TRC|DBG|INF|WRN|ERR|FTL)]`)
	return re.MatchString(text)
}

func SplitLogLine(text string) []string {
	re := regexp.MustCompile(`<[~*=-]*>`)
	return re.Split(text, -1)
}

func CountQuotedPasswords(lines []string) int {
	result := 0
	re := regexp.MustCompile(`(?i)".*password.*"`)
	for _, l := range lines {
		if re.MatchString(l) {
			result++
		}
	}
	return result
}

func RemoveEndOfLineText(text string) string {
	re := regexp.MustCompile(`end-of-line\d+`)
	return re.ReplaceAllString(text, "")
}

func TagWithUserName(lines []string) []string {
	var result []string
	re := regexp.MustCompile(`User\s+(\w+)`)
	for _, l := range lines {
		g := re.FindStringSubmatch(l)
		if g == nil {
			result = append(result, l)
		} else {
			fLine := fmt.Sprintf("[USR] %v %v", g[1], l)
			result = append(result, fLine)
		}
	}
	return result
}
