package airportrobot

import "fmt"

type Greeter interface {
	LanguageName() string
	Greet(name string) string
}

type Italian struct{}

func (greeter Italian) LanguageName() string {
	return "Italian"
}

func (greeter Italian) Greet(name string) string {
	return fmt.Sprintf("Ciao %v!", name)
}

type Portuguese struct{}

func (greeter Portuguese) LanguageName() string {
	return "Portuguese"
}

func (greeter Portuguese) Greet(name string) string {
	return fmt.Sprintf("Olá %v!", name)
}

func SayHello(name string, greeter Greeter) string {
	return fmt.Sprintf("I can speak %v: %v", greeter.LanguageName(), greeter.Greet(name))
}
