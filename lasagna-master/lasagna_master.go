package lasagna

func PreparationTime(layers []string, layerTime int) int {
	if layerTime == 0 {
		layerTime = 2
	}
	return len(layers) * layerTime
}

func Quantities(layers []string) (noodles int, sauce float64) {
	for _, l := range layers {
		if l == "noodles" {
			noodles += 50
		} else if l == "sauce" {
			sauce += 0.2
		}
	}
	return
}

func AddSecretIngredient(friendList []string, myList []string) {
	myList[(len(myList) - 1)] = friendList[(len(friendList) - 1)]
}

func ScaleRecipe(amounts []float64, portionCount int) []float64 {
	result := make([]float64, len(amounts))
	for i, amount := range amounts {
		result[i] = amount * float64(portionCount) / 2
	}
	return result
}
