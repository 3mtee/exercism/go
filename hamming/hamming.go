package hamming

import "errors"

func Distance(a, b string) (int, error) {
	if len(a) != len(b) {
		return 0, errors.New("strain lengths differ")
	}
	result := 0
	for i, r := range a {
		if r != int32(b[i]) {
			result++
		}
	}
	return result, nil
}
