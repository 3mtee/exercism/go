package chessboard

type File []bool

type Chessboard map[string]File

// CountInFile returns how many squares are occupied in the chessboard,
// within the given file.
func CountInFile(cb Chessboard, file string) int {
	result := 0
	for _, occupied := range cb[file] {
		if occupied {
			result++
		}
	}
	return result
}

// CountInRank returns how many squares are occupied in the chessboard,
// within the given rank.
func CountInRank(cb Chessboard, rank int) int {
	result := 0
	if rank < 1 || rank > 8 {
		return result
	}
	for _, file := range cb {
		if file[rank-1] {
			result++
		}
	}
	return result
}

// CountAll should count how many squares are present in the chessboard.
func CountAll(cb Chessboard) int {
	result := 0
	for _, file := range cb {
		for range file {
			result++
		}
	}
	return result
}

// CountOccupied returns how many squares are occupied in the chessboard.
func CountOccupied(cb Chessboard) int {
	result := 0
	for _, file := range cb {
		for _, occupied := range file {
			if occupied {
				result++
			}
		}
	}
	return result
}
