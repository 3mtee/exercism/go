package luhn

import (
	"strings"
	"unicode"
)

func Valid(id string) bool {
	id = strings.ReplaceAll(id, " ", "")
	length := len(id)
	if length <= 1 {
		return false
	}

	result := 0

	runes := []rune(id)
	for i := 0; i < length; i++ {
		r := runes[length-i-1]
		if !unicode.IsDigit(r) {
			return false
		}
		n := int(r - '0')
		if i%2 != 0 {
			n = n * 2
			if n > 9 {
				n -= 9
			}
		}
		result += n
	}

	return result%10 == 0
}
