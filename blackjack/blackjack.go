package blackjack

var cardValueMap = map[string]int{
	"ace":   11,
	"king":  10,
	"queen": 10,
	"jack":  10,
	"ten":   10,
	"nine":  9,
	"eight": 8,
	"seven": 7,
	"six":   6,
	"five":  5,
	"four":  4,
	"three": 3,
	"two":   2,
}

// ParseCard returns the integer value of a card following blackjack ruleset.
func ParseCard(card string) int {
	return cardValueMap[card]
}

// FirstTurn returns the decision for the first turn, given two cards of the
// player and one card of the dealer.
func FirstTurn(card1, card2, dealerCard string) string {
	var result string
	pScore := ParseCard(card1) + ParseCard(card2)
	dScore := ParseCard(dealerCard)
	switch {
	case card1 == card2 && card1 == "ace":
		result = "P"
	case pScore == 21 && dScore < 10:
		result = "W"
	case pScore == 21 && dScore >= 10:
		result = "S"
	case pScore >= 17 && pScore <= 20:
		result = "S"
	case pScore >= 12 && pScore <= 16 && dScore >= 7:
		result = "H"
	case pScore >= 12 && pScore <= 16 && dScore < 7:
		result = "S"
	case pScore <= 11:
		result = "H"
	}
	return result
}
