package collatzconjecture

import "errors"

func CollatzConjecture(n int) (int, error) {
	if n <= 0 {
		return 0, errors.New("incorrect input")
	}
	counter := getCounter()
	return counter(n), nil

}

func getCounter() func(start int) int {
	steps := 0
	// variable is necessary for recursion
	var counterFn func(int) int
	counterFn = func(start int) int {
		if start == 1 {
			return steps
		}

		steps++
		if start%2 == 0 {
			return counterFn(start / 2)
		} else {
			return counterFn(start*3 + 1)
		}
	}
	return counterFn
}
